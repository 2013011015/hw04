// minesweeper.cc
#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include "minesweeper.h"
using namespace std;
Minesweeper::Minesweeper()
{
	print.clear();
	width_=0;
	height_=0;
}
Minesweeper::~Minesweeper()
{
}
bool Minesweeper::SetMap(size_t w,size_t h, const vector<string>& map)
{
	int i,j,emp;
	print.resize(h+2);
	map_.resize(h);
	width_=w;
	height_=h;
	for (i=0;i<h;i++)
	{
		for (j=0;j<w;j++)
		{
			if (map[i][j]!='.' && map[i][j]!='*')
				return false;
		}
	}
	for (i=0;i<h+2;i++)
	{
		print[i].resize(w+2,'0');
	}
	for(i=0;i<h+2;i++)
	{
		for (j=0;j<w+2;j++)
			print[i][j]='0';
	}
	for(i=0;i<h;i++)
	{
		map_[i].resize(w);
	}
	for(i=0;i<h;i++)
	{
		for(j=0;j<w;j++)
			map_[i][j]=map[i][j];
	}
	for(i=0;i<h;i++)
	{
		for(j=0;j<w;j++)
		{
			if (map[i][j]=='*')
			{
				print[i+1][j+1]='*';
				print[i+2][j+1]++;
				print[i+1][j+2]++;
				print[i][j+1]++;
				print[i+1][j]++;
				print[i+2][j+2]++;
				print[i][j+2]++;
				print[i+2][j]++;
				print[i][j]++;
			}
		}
	}
	return true;
}

bool Minesweeper::ToggleMine(int x, int y)
{
	if (x>width_||y>height_||x<0||y<0)
		return false;
	else
	{
		if (map_[x][y]=='*')
		{
			map_[x][y]='.';
			SetMap(width_,height_,map_);
			return true;
		}
		else
		{
			map_[x][y]='*';
			SetMap(width_,height_,map_);
			return true;
		}
	}
}
