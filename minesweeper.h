// minesweeper.h

#ifndef _MINESWEEPER_H_
#define _MINESWEEPER_H_

#include <string>
#include <vector>
using namespace std;
class Minesweeper {
 public:
  Minesweeper();

  ~Minesweeper();

  bool SetMap(size_t w, size_t h, const std::vector<std::string>& map);
  bool ToggleMine(int x, int y);

  size_t width() const{return width_;}
  size_t height() const{return height_;}
  char get(int x, int y) const{return print[y+1][x+1];}

 private:
 vector<string> map_;
 vector<string> print;
 size_t width_;
 size_t height_;
};

#endif  // _MINESWEEPER_H_
