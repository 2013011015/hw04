// simple_int_set.cc
#include "simple_int_set.h"
SimpleIntSet::SimpleIntSet() {}
SimpleIntSet::~SimpleIntSet() {}
SimpleIntSet::SimpleIntSet(const SimpleIntSet& int_set)
{
        for(int i=0;i<size();i++)
                values_[i]=int_set.values_[i];
}
SimpleIntSet SimpleIntSet::Intersect(const SimpleIntSet& int_set) const
{
        int i,j,arrl=0,min=values_[0],max=0;
        int* arr;
        SimpleIntSet c;
        arr = new int [size()];
        for (i=0;i<size();i++)
        {
                if ( min> values_[i])
                        min=values_[i];
        }
        for (i=0;i<size();i++)
        {
                if (max< values_[i])
                        max=values_[i];
        }
        for(i=0;i<int_set.size();i++)
        {
                if ( min> int_set.values_[i])
                        min=int_set.values_[i];
        }
        for(i=0;i<int_set.size();i++)
        {
                if (max< int_set.values_[i])
                        max=int_set.values_[i];
        }
        for (i=min;i<=max;i++)
        {
                int count=0;
                for (j=0;j<size();j++)
                {
                        if (i==values_[j])
                        {
                                count++;
                                for(int k=0;k<int_set.size();k++)
                                {
                                        if(i==int_set.values_[k])
                                                count++;
                                }
                        }
                }
                if (count==2)
                {
                        arr[arrl]=i;
                        arrl++;
                }
        }
        c.Set(arr,arrl);
        return c;
}       
SimpleIntSet SimpleIntSet::Union(const SimpleIntSet& int_set) const
{
        int i,j,arrl=0,min=values_[0],max=0;
        int* arr;
        SimpleIntSet c;
        arr = new int [size()+int_set.size()];
        for (i=0;i<size();i++)
        {
                if ( min> values_[i])
                        min=values_[i];
        }
        for (i=0;i<size();i++)
        {
                if (max< values_[i])
                        max=values_[i];
        }
        for(i=0;i<int_set.size();i++)
        {
                if ( min> int_set.values_[i])
                        min=int_set.values_[i];
        }
        for(i=0;i<int_set.size();i++)
        {
                if (max< int_set.values_[i])
                        max=int_set.values_[i];
        }
        for (i=min;i<=max;i++)
        {
                int count=0;
                for (j=0;j<size();j++)
                {
                        if (i==values_[j])
                                count++;
                }
                for(j=0;j<int_set.size();j++)
                {
                        if (i==int_set.values_[j])
                                count++;
                }
                if (count>0)
                {
                        arr[arrl]=i;
                        arrl++;
                }
        }
        c.Set(arr,arrl);
        return c;
}
SimpleIntSet SimpleIntSet::Difference(const SimpleIntSet& int_set) const
{
        int i,j,arrl=0,min=values_[0],max=0;
        int* arr;
        SimpleIntSet c;
        arr = new int [values_.size()];
        for (i=0;i<size();i++)
        {
                if ( min> values_[i])
                        min=values_[i];
        }
        for (i=0;i<size();i++)
        {
                if (max< values_[i])
                        max=values_[i];
        }
        for(i=0;i<int_set.size();i++)
        {
                if ( min> int_set.values_[i])
                        min=int_set.values_[i];
        }
        for(i=0;i<int_set.size();i++)
        {
                if (max< int_set.values_[i])
                        max=int_set.values_[i];
        }
        for (i=min;i<=max;i++)
        {
                int count=0;
                for (j=0;j<size();j++)
                {
                        if (i==values_[j])
                        {
                                count++;
                                for (int k=0;k<int_set.size();k++)
                                {
                                        if(i==int_set.values_[k])
                                                count++;
                                }
                        }
                }
                if (count==1)
                {
                        arr[arrl]=i;
                        arrl++;
                }
        }
        c.Set(arr,arrl);
        return c;
}
void SimpleIntSet::Set(const int* values, size_t size)
{
        int i,j,tmp;
        values_.resize(size);
        for (i=0;i< size;i++)
                values_[i]=values[i];
        for (i=0;i<size;i++)
        {
                for(j=0;j<size;j++)
                {
                        if (values_[i]<values_[j])
                        {
                                tmp=values_[i];
                                values_[i]=values_[j];
                                values_[j]=tmp;
                        }
                }
        }
}

