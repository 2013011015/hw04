// vending_machine.cc

#include "vending_machine.h"
using namespace std;

void VendingMachine::AddBeverage(const string& name,int unit_price,int stock)
{       
	if(beverages_.find(name)==beverages_.end())
	{
        	BeverageInfo chooga;
        	chooga.name = name;
	        chooga.unit_price = unit_price;
	        chooga.stock = stock;
        	beverages_.insert(make_pair(name,chooga));
	}
	else
	{
		beverages_[name].unit_price=unit_price;
		beverages_[name].stock+=stock;
	}
}
int VendingMachine::Buy(const map<string,int>& items, int money)
{
	int total_money=0;
	map<string,int>::const_iterator it;
	for (it=items.begin();it!=items.end();it++)
	{
	
		if(beverages_.find(it->first)==beverages_.end())
			return -3;
	}
	for (it=items.begin();it!=items.end();it++)
		total_money+=beverages_[it->first].unit_price*(it->second);
	if(total_money>money)
		return -1;
	for (it=items.begin();it!=items.end();it++)
	{
		if(beverages_[it->first].stock<it->second)
			return -2;
	}
	for (it=items.begin();it!=items.end();it++)
		beverages_[it->first].stock-=it->second;
	return money-total_money;
}

